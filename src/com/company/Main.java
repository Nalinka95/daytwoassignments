package com.company;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.w3c.dom.ls.LSOutput;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
	//....check optional.....
     /*   Optional<Employee> employee1= Optional.empty();
        Employee em1= new Employee("Sehansa heshan",30000);
        checkNameOfEmployee(em1,"S");
*/
     //..forEach
       /* ArrayList<String> arrayList= new ArrayList<>(Arrays.asList("AAA","ABC","abA"));
        forEachConsumer(arrayList);*/

       //Stream for accumulate
/*
        ArrayList<Integer> numbersList= new ArrayList<>(Arrays.asList(0,1,2,3));
        accumulateInteger(numbersList);
        */

        // stream for revised array
        /*ArrayList<Integer> numbersList= new ArrayList<>(Arrays.asList(0,1,2,3));
        reversedList(numbersList);
        */
        //Inheritance
        Test3 obj = new Test3();
        obj.eat();

    }
    public static void lambdaWithFor(LinkedList<Integer> numbersList){
        ShowEven reminder= input->(input%2==0);
        for(Integer element: numbersList){
            if(reminder.isEqual(element)){
                System.out.print(element+" ");
            }
        }
    }
    public static void accumulateInteger(ArrayList<Integer> numbersList){
        Stream<Integer> processingData= numbersList.stream();
        System.out.println(processingData.reduce((acc,item)->acc+item).orElse(0));
    }
    public static void reversedList(ArrayList list){
        Stream<Integer> processingData= list.stream();
        processingData.sorted(Collections.reverseOrder()).forEach(System.out::println);
    }
    public static void forEachConsumer(ArrayList<String> wordList){
        Consumer<String> consumer=s->{
            StringBuilder input1 = new StringBuilder();
            input1.append(s);
            input1 = input1.reverse();
            if(s.equals(input1.toString())){
                System.out.println(s+" is a palindrome  ");
            }
            else{
                System.out.println(s+" is not a palindrome  ");
            }
        };
        wordList.forEach(consumer);
    }
    public static void checkNameOfEmployee(Employee employee, String startingValue){
        Optional<Employee>employeeOptional=Optional.of(employee);
        Predicate<Employee> p= x->(x.getName().startsWith("s"));
        System.out.println(employeeOptional.filter(p).orElse(new Employee("Seshan", 23223)));

    }
}
interface ShowEven{

    boolean isEqual(int number);
}
class Employee{
    public String name;
    public int salary;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
// working with default
interface m1 {
    default void eat() {
        System.out.println("Eating in m1");
    }
}
interface m2 {
    default void eat() {
        System.out.println("Eating in m2");
    }
}
class Test3 implements m1, m2{
    @Override
    public void eat() {
        m2.super.eat();
    }
}